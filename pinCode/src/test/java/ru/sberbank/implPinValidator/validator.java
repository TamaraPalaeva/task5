package ru.sberbank.implPinValidator;

import org.junit.Assert;
import org.junit.Test;
import ru.sberbank.entity.Client;

public class validator {

    @Test
    public void validate(){
        Client client = new Client("Ivanod", "1", "9876", 6542);

        boolean testResult = new PinValidatorService().validator(client, "9876");

        Assert.assertTrue(testResult);

    }

}
