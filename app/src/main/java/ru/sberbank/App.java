package ru.sberbank;

import ru.sberbank.implPinValidator.PinValidatorService;
import ru.sberbank.implServer.TerminalServer;
import ru.sberbank.implService.TerminalService;

public class App {
    public static void main(String[] args) throws Exception{
        TerminalService service = new TerminalService(new TerminalServer(), new PinValidatorService());

        try {
            service.login("1", "1234");
            service.login("1", "1234");
            service.login("1", "1234");
            service.login("1", "1234");
        } catch (Exception e) {
            System.err.println(e.getMessage()) ;
        }

        service.login("1", "1234");

        Thread.sleep(6000);

        service.login("1", "9876");

        service.presetSummAccount("1");

        service.creditOfAccount("1", 2541);
        service.creditOfAccount("1", 25000);
        service.creditOfAccount("1", 2500);

        service.depositOfAccount("1", 2451);
        service.depositOfAccount("1", 7100);

    }

}
