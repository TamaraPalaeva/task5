package ru.sberbank.exceptions;

public class TransitException extends RuntimeException {

    public TransitException(String message) {
        super(message);
    }

}

