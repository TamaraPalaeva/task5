package ru.sberbank.implPinValidator;

import ru.sberbank.entity.Client;
import ru.sberbank.exceptions.AccountException;
import ru.sberbank.service.PinValidator;
import java.util.Date;

public class PinValidatorService implements PinValidator {


    public boolean validator(Client client, String pin) throws AccountException {
        boolean pinIsValid = client.getPinCode().equals(pin);
        if (pinIsValid) {
            client.setAttempts(3);
            client.setLoginClient(true);
            System.out.println("Авторизация прошла успешно");
            return true;
        } else {
            if (client.getAttempts() > 0){
                client.setAttempts(client.getAttempts() - 1);
                return false;
            }
            else {
                client.setDateFinishedBlock(new Date(new Date().getTime() + 5000L));
                throw new AccountException("Клиент заблокирован на 5 сек");
            }
        }
    }
}
