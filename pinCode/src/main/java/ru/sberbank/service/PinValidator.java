package ru.sberbank.service;


import ru.sberbank.entity.Client;
import ru.sberbank.exceptions.AccountException;

public interface PinValidator {

    boolean validator(Client client, String pin) throws AccountException;

}
