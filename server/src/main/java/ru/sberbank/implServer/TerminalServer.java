package ru.sberbank.implServer;

import ru.sberbank.entity.Client;
import ru.sberbank.exceptions.AccountException;
import ru.sberbank.exceptions.TransitException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TerminalServer {
    private Map<String, Integer> accounts = new HashMap<>();
    private Map<String, Client> clientMap = new HashMap<>();

    {
        clientMap.put("1", new Client("Ivanod", "1", "9876", 6542));
        clientMap.put("2", new Client("Petrov", "2", "7412", 254010));
        clientMap.put("3", new Client("Sidorov", "3", "6523", 685300));

    }

    public Integer getSummAccount(String idClient) {
        return clientMap.get(idClient).getSummAccount();
    }

    public void creditAccount(String idClient, Integer summTransfer) {
        if (summTransfer % 100 != 0) {
            throw new TransitException("Сумма не кратна 100");
        } else if (summTransfer > clientMap.get(idClient).getSummAccount()) {
            throw new TransitException("Сумма снятия превышает остаток по счету");
        }
        else {
            int summAccount = clientMap.get(idClient).getSummAccount();
            clientMap.get(idClient).setSummAccount(summAccount - summTransfer);
            System.out.println("Операция прошла успешно. Остаток по счету: " + clientMap.get(idClient).getSummAccount() + "руб");
        }
    }

    public void depositAccount(String idClient, Integer summTransfer) {
        if (summTransfer % 100 != 0) {
            throw new TransitException("Сумма не кратна 100");
        } else {
            int summAccount = clientMap.get(idClient).getSummAccount();
            clientMap.get(idClient).setSummAccount(summAccount + summTransfer);
            System.out.println("Операция прошла успешно. Остаток по счету: " + clientMap.get(idClient).getSummAccount() + "руб");
        }
    }

    public Client getClientById(String idClient) throws AccountException {
        Client client = clientMap.get(idClient);
        if (client.getDateFinishedBlock() != null && new Date().getTime() < client.getDateFinishedBlock().getTime()) {
            throw new AccountException("Клиент заблокирован");
        }
        return client;
    }
}

